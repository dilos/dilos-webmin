#!/bin/sh
#
# Copyright (c) 2012-2018, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. /lib/svc/share/smf_include.sh

result=${SMF_EXIT_OK}
CFG=/etc/webmin/miniserv.conf
for i in 1 2 3 4 5 6 7 8 9 10; do
	[ -f ${CFG} ] && break;
	sleep 3
done
if [ ! -f ${CFG} ]; then
	echo "No config file ${CFG}"
	exit 1
fi

PIDFILE=`grep "^pidfile=" ${CFG} | sed -e 's/pidfile=//g'`
VARDIR=/var/webmin
LANG=
export LANG
#PERLIO=:raw
unset PERLIO
export PERLIO
PERLLIB=/usr/share/webmin
export PERLLIB
KFILE=/etc/webmin/miniserv.pem

ssl_gen()
{
	local cert=/tmp/cert.$$
	local key=/tmp/key.$$
	local host=`hostname`

	openssl req -newkey rsa:2048 -x509 -nodes -out ${cert} -keyout ${key} -days 1825 -sha256 >/dev/null 2>&1 <<EOF
.
.
.
Webmin Webserver on ${host}
.
*
root@${host}
EOF
	if [ $? -eq 0 ]; then
		cat ${cert} ${key} >${KFILE}
		chmod 600 ${KFILE}
	fi
	rm -f ${cert} ${key}
}

# Read command line arguments
method="$1"		# %m
instance="$2" 		# %i

case "$method" in
'start')
    rm -f $PIDFILE
    if [ ! -d ${VARDIR} ]; then
	mkdir -p ${VARDIR}
	chown root:bin ${VARDIR}
	chmod 700 ${VARDIR}
    fi
    if [ ! -f ${KFILE} ]; then
	ssl_gen
    fi
    PATH=/usr/bin:/sbin:/usr/sbin /usr/share/webmin/miniserv.pl ${CFG}&
    result=$?
    ;;

'refresh')
    [ ! -f "$PIDFILE" ] && exit 0
    pid=`cat $PIDFILE`
    kill -USR1 $pid
    ;;

'stop')
    [ ! -f "$PIDFILE" ] && exit 0
    pid=`cat $PIDFILE`
    kill -HUP $pid
    ;;

*)
	echo "Usage: $I [stop|start] <instance>" >&2
	exit 1
	;;
esac

exit ${result}
