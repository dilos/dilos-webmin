#!/usr/local/bin/perl
# Show RBAC status

use strict;
use warnings;
require './acl-lib.pl';
our (%in, %text, %gconfig, %access, $module_name, $module_root_directory);
$access{'rbacenable'} || &error($text{'rbac_ecannot'});
&ui_print_header(undef, $text{'rbac_title'}, "");

print "$text{'rbac_desc'}<p>\n";
if ($gconfig{'os_type'} ne 'dilos') {
	print &text('rbac_esolaris', $gconfig{'real_os_type'}),"<p>\n";
	}
else {
	print "$text{'rbac_ok'}<p>\n";
	}

&ui_print_footer("", $text{'index_return'});

